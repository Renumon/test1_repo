const path = require('path')
const webpack = require('webpack')
const nodeExternals = require('webpack-node-externals')
module.exports = {
  entry: {    
    server: {
      import:['./src/server/index.ts'],
    }
  },
  output: {
    path: path.join(__dirname, 'server'),
    publicPath: '/public',
    filename: '[name].js'
  },
  target: 'node',
  resolve: {
    extensions: ['.ts', '.js', '.json']
  },
  node: {
    // Need this when working with express, otherwise the build fails
    __dirname: false,   // if you don't put this is, __dirname
    __filename: false,  // and __filename return blank or /
  },
  externals: [nodeExternals()], // Need this to avoid error when working with Express
  module: {
    rules: [
        {
            test: /\.tsx?$/,
            use: 'ts-loader',
            exclude: /node_modules/,
          }
    ]
  },
  plugins: [

  ]
}