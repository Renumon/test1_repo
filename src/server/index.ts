import express from 'express';
import * as path from 'path'
import { env } from "process";



let environment:string = process.env.NODE_ENV || '';



(async function(){

    const app = express();  


    (function () {
        // Step 1: Create & configure a webpack compiler
        let webpack = require('webpack');
        let webpackConfig = require('../../webpack.client.config');
        let compiler = webpack(webpackConfig);
        const path = require('path');
      
        // Step 2: Attach the dev middleware to the compiler & the server
        app.use(
          require('webpack-dev-middleware')(compiler, {
            publicPath: webpackConfig.output.publicPath
          })
        );
      
        // Step 3: Attach the hot middleware to the compiler & the server
        app.use(
          require('webpack-hot-middleware')(compiler, {
            log: console.log,
            path: '/__webpack_hmr',
            heartbeat: 10 * 1000,
          })
        );
      })();

        

    
    app.use('/css',express.static('./public/css'))
    app.get("*", function(req:any, res:any) {    
        if(req.hostname.indexOf('adfyn.com')>-1){
          res.sendFile(req.url, { root: './adfyn' });          
        }else{
          res.sendFile('index.html', { root: './public' });
        }
    });
               
    app.listen(9005);    

    console.log(`Listening on port ${9005}`);

})();
