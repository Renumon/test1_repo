
import { createApp } from "vue"
import root from './components/root.vue'
import { router } from './routes'
//import React, { Component } from 'react'
import Icon from '@mdi/react'
import { mdiAccount } from '@mdi/js'; 
//import GoogleAuth from '@/config/google_oAuth.js'
//const gauthOption = {
 //// clientId: '707231563844-e5cpkqrlt62gncmj6b84of5sml9lp8g9.apps.googleusercontent.com',
  ////scope: 'profile email',
  //prompt: 'select_account'
//}
//createApp(root).use(GoogleAuth, gauthOption)
//root.config.productionTip = false


createApp(root).use(router).mount('#app');
