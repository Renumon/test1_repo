import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import home from '../components/user/home.vue'
import login from '../components/auth/login.vue'
import register from '../components/auth/register.vue'


const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'home',
    component: home
  },
  {
    path: '/search',
    name: 'search',    
    component: () => import('../components/user/search.vue')
  },
  {
    path: '/login',
    name: 'login',
    component: login
  },
  {
    path: '/register',
    name: 'register',
    component: register
  }
]

const router = createRouter({
  history: createWebHistory('/'),
  routes
})

export {router}
