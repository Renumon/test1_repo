import { reactive, computed } from 'vue';
import { User } from '../types/user';
import { DLP } from '../providers/dlp';

interface State {
  list: User[];
}

export function useUsers() {
  const state: State = reactive({
    list: [],
    listLength: computed(() => state.list.length),
  });

  async function get() {
    let collection=await DLP.Collections<User>("dlp_user");      
    state.list = await collection?.find({});            
  }

  return {
    state,
    get,
  };
}