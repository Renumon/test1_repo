import * as Realm from "realm-web"
import { Services } from "realm"

export class DLP {

    static dlp:{ [id: string] : Services.MongoDB.MongoDBCollection<any>; } = {};;

    static async Collections<T extends Services.MongoDB.Document<any>>(collectionName:string):Promise<Services.MongoDB.MongoDBCollection<T>>{

        if(this.dlp[collectionName])return this.dlp[collectionName];

        let monapp:Realm.App = new Realm.App({id:'dlp-novqb'});    
        const credentials = Realm.Credentials.anonymous();
        try {
            const user = await monapp.logIn(credentials);  
            let mongodb = await monapp.currentUser?.mongoClient('mongodb-atlas');
            let db = await mongodb?.db("DLP");
            let collection = await db?.collection<T>(collectionName);
            if(collection)
                this.dlp[collectionName] = collection;
            return this.dlp[collectionName];
        }catch(e){}
        return new Promise((res,rej)=>{ rej(); });
    }
}

