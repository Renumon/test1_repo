const path = require("path");
const { VueLoaderPlugin } = require("vue-loader");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const webpack = require('webpack')

module.exports = {
  mode:  "development",
  devtool: "source-map",
  entry: {
    index:{
      import:['webpack-hot-middleware/client?name=index','./src/client/index.ts'],
    }
  },
  output: {
    path: path.resolve(__dirname, "./public"),
    publicPath: "/"
  },
  resolve: {
    extensions: [ '.tsx', '.ts', '.js','.vue' ],
    alias: {
      vue: "@vue/runtime-dom"
    }
  },
  module: {
    rules: [
        {
            test: /\.vue$/,
            loader: "vue-loader"
        },
        {
            test: /\.js$/,
            loader: 'babel-loader',
            include: /src/
        },
        {
          test: /\.html$/,
          loader: 'raw-loader',
          exclude: /node_modules/,        
        },
        {
          test: /\.png$/,
          loader:  "url-loader"
        },
        {
          test: /\.css$/,
          loader: 'css-loader',
          exclude: /node_modules/,        
        },
        {
          test: /\.tsx?$/,
          loader: 'ts-loader',
          options: {
            appendTsSuffixTo: [/\.vue$/],
          },
          exclude: /node_modules/,        
        }
      ]
  },
  plugins: [
    new VueLoaderPlugin(),
    new MiniCssExtractPlugin({
      filename: "[name].css"
    }),
    new webpack.HotModuleReplacementPlugin()
  ],
  devServer: {
    hot: true    
  }
};
