/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./src/server/index.ts":
/*!*****************************!*\
  !*** ./src/server/index.ts ***!
  \*****************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";
eval("\r\nvar __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {\r\n    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }\r\n    return new (P || (P = Promise))(function (resolve, reject) {\r\n        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }\r\n        function rejected(value) { try { step(generator[\"throw\"](value)); } catch (e) { reject(e); } }\r\n        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }\r\n        step((generator = generator.apply(thisArg, _arguments || [])).next());\r\n    });\r\n};\r\nvar __generator = (this && this.__generator) || function (thisArg, body) {\r\n    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;\r\n    return g = { next: verb(0), \"throw\": verb(1), \"return\": verb(2) }, typeof Symbol === \"function\" && (g[Symbol.iterator] = function() { return this; }), g;\r\n    function verb(n) { return function (v) { return step([n, v]); }; }\r\n    function step(op) {\r\n        if (f) throw new TypeError(\"Generator is already executing.\");\r\n        while (_) try {\r\n            if (f = 1, y && (t = op[0] & 2 ? y[\"return\"] : op[0] ? y[\"throw\"] || ((t = y[\"return\"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;\r\n            if (y = 0, t) op = [op[0] & 2, t.value];\r\n            switch (op[0]) {\r\n                case 0: case 1: t = op; break;\r\n                case 4: _.label++; return { value: op[1], done: false };\r\n                case 5: _.label++; y = op[1]; op = [0]; continue;\r\n                case 7: op = _.ops.pop(); _.trys.pop(); continue;\r\n                default:\r\n                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }\r\n                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }\r\n                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }\r\n                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }\r\n                    if (t[2]) _.ops.pop();\r\n                    _.trys.pop(); continue;\r\n            }\r\n            op = body.call(thisArg, _);\r\n        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }\r\n        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };\r\n    }\r\n};\r\nvar __importDefault = (this && this.__importDefault) || function (mod) {\r\n    return (mod && mod.__esModule) ? mod : { \"default\": mod };\r\n};\r\nObject.defineProperty(exports, \"__esModule\", ({ value: true }));\r\nvar express_1 = __importDefault(__webpack_require__(/*! express */ \"express\"));\r\nvar environment = \"development\" || 0;\r\n(function () {\r\n    return __awaiter(this, void 0, void 0, function () {\r\n        var app;\r\n        return __generator(this, function (_a) {\r\n            app = (0, express_1.default)();\r\n            (function () {\r\n                // Step 1: Create & configure a webpack compiler\r\n                var webpack = __webpack_require__(/*! webpack */ \"webpack\");\r\n                var webpackConfig = __webpack_require__(/*! ../../webpack.client.config */ \"./webpack.client.config.js\");\r\n                var compiler = webpack(webpackConfig);\r\n                var path = __webpack_require__(/*! path */ \"path\");\r\n                // Step 2: Attach the dev middleware to the compiler & the server\r\n                app.use(__webpack_require__(/*! webpack-dev-middleware */ \"webpack-dev-middleware\")(compiler, {\r\n                    publicPath: webpackConfig.output.publicPath\r\n                }));\r\n                // Step 3: Attach the hot middleware to the compiler & the server\r\n                app.use(__webpack_require__(/*! webpack-hot-middleware */ \"webpack-hot-middleware\")(compiler, {\r\n                    log: console.log,\r\n                    path: '/__webpack_hmr',\r\n                    heartbeat: 10 * 1000,\r\n                }));\r\n            })();\r\n            app.use('/css', express_1.default.static('./public/css'));\r\n            app.get(\"*\", function (req, res) {\r\n                if (req.hostname.indexOf('adfyn.com') > -1) {\r\n                    res.sendFile(req.url, { root: './adfyn' });\r\n                }\r\n                else {\r\n                    res.sendFile('index.html', { root: './public' });\r\n                }\r\n            });\r\n            app.listen(9005);\r\n            console.log(\"Listening on port \" + 9005);\r\n            return [2 /*return*/];\r\n        });\r\n    });\r\n})();\r\n\n\n//# sourceURL=webpack://vuetest/./src/server/index.ts?");

/***/ }),

/***/ "./webpack.client.config.js":
/*!**********************************!*\
  !*** ./webpack.client.config.js ***!
  \**********************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

eval("const path = __webpack_require__(/*! path */ \"path\");\r\nconst { VueLoaderPlugin } = __webpack_require__(/*! vue-loader */ \"vue-loader\");\r\nconst MiniCssExtractPlugin = __webpack_require__(/*! mini-css-extract-plugin */ \"mini-css-extract-plugin\");\r\nconst webpack = __webpack_require__(/*! webpack */ \"webpack\")\r\n\r\nmodule.exports = {\r\n  mode:  \"development\",\r\n  devtool: \"source-map\",\r\n  entry: {\r\n    index:{\r\n      import:['webpack-hot-middleware/client?name=index','./src/client/index.ts'],\r\n    }\r\n  },\r\n  output: {\r\n    path: path.resolve(__dirname, \"./public\"),\r\n    publicPath: \"/\"\r\n  },\r\n  resolve: {\r\n    extensions: [ '.tsx', '.ts', '.js','.vue' ],\r\n    alias: {\r\n      vue: \"@vue/runtime-dom\"\r\n    }\r\n  },\r\n  module: {\r\n    rules: [\r\n        {\r\n            test: /\\.vue$/,\r\n            loader: \"vue-loader\"\r\n        },\r\n        {\r\n            test: /\\.js$/,\r\n            loader: 'babel-loader',\r\n            include: /src/\r\n        },\r\n        {\r\n          test: /\\.html$/,\r\n          loader: 'raw-loader',\r\n          exclude: /node_modules/,        \r\n        },\r\n        {\r\n          test: /\\.png$/,\r\n          loader:  \"url-loader\"\r\n        },\r\n        {\r\n          test: /\\.css$/,\r\n          loader: 'css-loader',\r\n          exclude: /node_modules/,        \r\n        },\r\n        {\r\n          test: /\\.tsx?$/,\r\n          loader: 'ts-loader',\r\n          options: {\r\n            appendTsSuffixTo: [/\\.vue$/],\r\n          },\r\n          exclude: /node_modules/,        \r\n        }\r\n      ]\r\n  },\r\n  plugins: [\r\n    new VueLoaderPlugin(),\r\n    new MiniCssExtractPlugin({\r\n      filename: \"[name].css\"\r\n    }),\r\n    new webpack.HotModuleReplacementPlugin()\r\n  ],\r\n  devServer: {\r\n    hot: true    \r\n  }\r\n};\r\n\n\n//# sourceURL=webpack://vuetest/./webpack.client.config.js?");

/***/ }),

/***/ "express":
/*!**************************!*\
  !*** external "express" ***!
  \**************************/
/***/ ((module) => {

"use strict";
module.exports = require("express");

/***/ }),

/***/ "mini-css-extract-plugin":
/*!******************************************!*\
  !*** external "mini-css-extract-plugin" ***!
  \******************************************/
/***/ ((module) => {

"use strict";
module.exports = require("mini-css-extract-plugin");

/***/ }),

/***/ "vue-loader":
/*!*****************************!*\
  !*** external "vue-loader" ***!
  \*****************************/
/***/ ((module) => {

"use strict";
module.exports = require("vue-loader");

/***/ }),

/***/ "webpack":
/*!**************************!*\
  !*** external "webpack" ***!
  \**************************/
/***/ ((module) => {

"use strict";
module.exports = require("webpack");

/***/ }),

/***/ "webpack-dev-middleware":
/*!*****************************************!*\
  !*** external "webpack-dev-middleware" ***!
  \*****************************************/
/***/ ((module) => {

"use strict";
module.exports = require("webpack-dev-middleware");

/***/ }),

/***/ "webpack-hot-middleware":
/*!*****************************************!*\
  !*** external "webpack-hot-middleware" ***!
  \*****************************************/
/***/ ((module) => {

"use strict";
module.exports = require("webpack-hot-middleware");

/***/ }),

/***/ "path":
/*!***********************!*\
  !*** external "path" ***!
  \***********************/
/***/ ((module) => {

"use strict";
module.exports = require("path");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module is referenced by other modules so it can't be inlined
/******/ 	var __webpack_exports__ = __webpack_require__("./src/server/index.ts");
/******/ 	
/******/ })()
;